<!DOCTYPE html>
<html>

<head>
    <title>หลักสูตร</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">หลักสูตรทั้งหมด</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li class="active">หลักสูตรทั้งหมด</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="d-flex row">
                <div class="col-lg-2 bg-filter">
                    <aside class="sidebar">
                        <h5 class="text-5 text-dark my-1 py-2">หมวดหมู่หลักสูตร</h5>
                        <div class="toggle toggle-minimal toggle-primary" data-plugin-toggle data-plugin-options="{ 'isAccordion': true }">
                            <section class="toggle active">
                                <a class="toggle-title text-4">ประเภท 1</a>
                                <div class="toggle-content">
                                    <p><a href="#">หลักสูตร 1</a></p>
                                    <p><a href="#">หลักสูตร 2</a></p>
                                    <p><a href="#">หลักสูตร 3</a></p>
                                </div>
                            </section>
                            <section class="toggle">
                                <a class="toggle-title text-4">ประเภท 2</a>
                                <div class="toggle-content">
                                    <p><a href="#">หลักสูตร 1</a></p>
                                    <p><a href="#">หลักสูตร 2</a></p>
                                    <p><a href="#">หลักสูตร 3</a></p>
                                </div>
                            </section>
                            <section class="toggle">
                                <a class="toggle-title text-4">ประเภท 3</a>
                                <div class="toggle-content">
                                    <p><a href="#">หลักสูตร 1</a></p>
                                    <p><a href="#">หลักสูตร 2</a></p>
                                    <p><a href="#">หลักสูตร 3</a></p>
                                </div>
                            </section>
                            <section class="toggle">
                                <a class="toggle-title text-4">ประเภท 4</a>
                                <div class="toggle-content">
                                    <p><a href="#">หลักสูตร 1</a></p>
                                    <p><a href="#">หลักสูตร 2</a></p>
                                    <p><a href="#">หลักสูตร 3</a></p>
                                </div>
                            </section>
                        </div>
                    </aside>
                </div>

                <div class="col-lg-10 section-allcourse">
                    <div class="row g-5">
                        <?php for ($i = 0; $i < 8; $i++) { ?>
                            <div class="col-sm-4 col-lg-3 ">
                                <div class="card card-course">
                                    <a href="#">
                                        <img class="card-img-top" src="img/1-main/thumbnail.png">
                                    </a>
                                    <div class="card-body">
                                        <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                        <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                        <hr class="mb-2">
                                        <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>


            </div>
        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>