<!DOCTYPE html>
<html>

<head>
    <title>กิจกรรมข่าวสาร</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">กิจกรรมข่าวสาร</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li class="active">กิจกรรมข่าวสาร</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>


            <div class="position-relative">

                <div class="bg-page">
                    <img src="img/1-main/main-bg-1.png">
                </div>

                <div class="container content">
                    <div class="row">
                        
                        <div class="col-sm-6 col-lg-3 my-3">
                            <div class="card card-news border">
                                <a href="#">
                                    <img class="card-img-top" src="img/1-main/thumbnail.png">
                                </a>
                                <div class="card-body">
                                    <h6 class="card-title"><a href="#">ชื่อกิจกรรมข่าวสาร</a></h6>
                                    <p class="text-color-muted"><i class="icon-calendar"></i> 10 เมษายน 2564</p>
                                    <a href="#" class="read-more float-end">อ่านเพิ่มเติม<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>

</body>

</html>