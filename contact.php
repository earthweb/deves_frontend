<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">ติดต่อเรา</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li class="active">ติดต่อเรา</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>


            <div class="position-relative">

                <div class="bg-page">
                    <img src="img/1-main/main-bg-1.png">
                </div>

                <div class="container content contact">
                    <div class="row mb-4 justify-content-center">
                        <div class="col-lg-6 px-0">
                            <div class="google-map my-0 h-100">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.370313137054!2d100.50238231465426!3d13.756527990344354!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e299151cb7ed07%3A0xd6b3ed7dcb891d9c!2z4Lia4Lih4LiILiDguYDguJfguYDguKfguKjguJvguKPguLDguIHguLHguJnguKDguLHguKIgKOC4quC4s-C4meC4seC4geC4h-C4suC4meC5g-C4q-C4jeC5iCk!5e0!3m2!1sth!2sth!4v1622628082804!5m2!1sth!2sth" width="100%" height="100%" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                        </div>
                        <div class="col-lg-6 px-0">
                            <div class="card border px-3">
                                <div class="row align-items-center border-bottom px-3 py-4">
                                    <div class="col-auto">
                                        <img src=".\img\1-main\pin-icon.png">
                                    </div>
                                    <div class="col">
                                        <h6>บริษัท เทเวศประกันภัย จำกัด (มหาชน) สำนักงานใหญ่</h6>
                                        <p>97 และ 99 อาคารเทเวศประกันภัย ถ.ราชดำเนินกลาง แขวงบวรนิเวศ เขตพระนคร กรุงเทพฯ 10200</p>
                                    </div>
                                </div>
                                <div class="row align-items-center px-3 py-2">
                                    <div class="col-auto">
                                        <img src=".\img\1-main\tel-icon.png">
                                    </div>
                                    <div class="col">
                                        <h6>โทรศัพท์</h6>
                                        <p>1291 ต่อ 8376, 8379, 8383</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>