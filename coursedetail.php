<!DOCTYPE html>
<html>

<head>
    <title>หลักสูตร</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .table-coursedetail {
            min-width: 500px;
        }

        .table-coursedetail tbody tr td:nth-child(1) {
            width: 20px;
        }

        .table-coursedetail tbody tr td:nth-child(3),
        .table-coursedetail tbody tr td:nth-child(4) {
            text-align: center;
            width: 150px;
        }

        .table-coursedetail tbody tr td button {
            width: 100%;
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">ชื่อหลักสูตร</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li><a href="#">หลักสูตรทั้งหมด</a></li>
                                <li class="active">หลักสูตร 1</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="course-menu">
                            <div class="row mb-4">
                                <div class="col">
                                    <div class="card">
                                        <a href="#"><img class="card-img-top" src="img/1-main/thumbnail.png" alt=""></a>
                                        <div class="card-body" style="padding:10px;">
                                            <p class="card-title mb-4 text-4 "><a href="#" style="text-decoration: none">ชื่อหลักสูตร</a></p>
                                            <hr>
                                            <div class="progress progress-sm progress-border-radius mb-2">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                    <span class="sr-only">60% Complete</span>
                                                </div>
                                            </div>
                                            <p class="card-text">0 % เสร็จสมบูรณ์ </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <div class="col">
                                    <div class="card">
                                        <div class="card-body row p-2">
                                            <div class="col border-end">
                                                <p class="mb-1 text-3">ระยะเวลา</p>
                                                <img class="mx-auto d-block mt-3 mb-2" src=".\img\1-main\clock-icon.png">
                                                <h6 class="text-3 text-center">2 ชั่วโมง</h6>
                                            </div>
                                            <div class="col ">
                                                <p class="mb-1 text-3">จำนวนบทเรียน</p>
                                                <img class="mx-auto d-block mt-3 mb-2" src=".\img\1-main\book-icon.png">
                                                <h6 class="text-3 text-center">4 บทเรียน</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <div class="col">
                                    <div class="card">
                                        <div class="card-body row p-2">
                                            <div class="col border-end">
                                                <p class="mb-1 text-3">สถานะ Certificate</p>
                                                <img class="mx-auto d-block mt-4 mb-3" src=".\img\1-main\certificate-icon.png">
                                                <a href="#" class="text-decoration-none"><button class="text-3 border border-2  btn mx-auto d-block mb-3" disabled>ส่งไปที่อีเมล</button></a>
                                            </div>
                                            <div class="col ">
                                                <p class="mb-1 text-3">แบบประเมินหลักสูตร</p>
                                                <img class="mx-auto d-block mt-4 mb-3" src=".\img\1-main\questionnaire-icon.png">
                                                <a href="./questionnaire.php" class="text-decoration-none"><button class="text-3 btn btn-main mx-auto d-block mb-3">ทำแบบประเมิน</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-12">
                                <div class="course-content">
                                    <div class="tabs tabs-bottom tabs-center tabs-simple">
                                        <ul class="nav nav-tabs justify-content-start">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#tabsNavigationSimple1" data-bs-toggle="tab">รายละเอียดหลักสูตร</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#tabsNavigationSimple2" data-bs-toggle="tab">รายการหลักสูตร</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content mt-4">
                                            <div class="tab-pane " id="tabsNavigationSimple1">
                                                <div>
                                                    <h4>Customer Support</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia.</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Nullam convallis, arcu vel pellentesque sodales, nisi est varius diam, ac ultrices sem ante quis sem. Proin ultricies volutpat sapien, nec scelerisque ligula mollis lobortis.</p>
                                                    <h4>HTML5 / CSS3 / JS</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia.</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Nullam convallis, arcu vel pellentesque sodales, nisi est varius diam, ac ultrices sem ante quis sem. Proin ultricies volutpat sapien, nec scelerisque ligula mollis lobortis.</p>
                                                </div>
                                            </div>
                                            <div class="tab-pane active" id="tabsNavigationSimple2">

                                                <div class="row">
                                                    <div class="col-sm-12 course-section">
                                                        <div class="section-title">
                                                            <h4> บทที่ 1</h4>
                                                        </div>
                                                        <ul class="section-list">
                                                            <li>
                                                                <a href=""><span class="staus-learn"><img src=".\img\1-main\check-icon.png"></span>ทำข้อสอบก่อนเรียน
                                                                    <div class="float-end"><span class="mx-2">คะแนนที่ได้</span>  <span  class="px-3 border border-2 rounded-3">8/15</span></div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href=""><span class="staus-learn"><img src=".\img\1-main\halfcheck-icon.png"></span>วิดีโอบทที่ 1
                                                                    <div class="float-end"><span class="mx-2"><i class="far fa-clock"></i> 30 นาที  </span><span  class="px-3 btn-main rounded-3">ดูวิดีโอ</span></div>
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a href=""><span class="staus-learn"><img src=".\img\1-main\uncheck-icon.png"></span>ทำหลังก่อนเรียน
                                                                    <div class="float-end"><span  class="px-3 btn-main rounded-3">ทำข้อสอบ</span></div>
                                                                </a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

    <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>