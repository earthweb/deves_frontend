<footer  class="border border-end-0 border-start-0 border-bottom-0 border-color-light-3 mt-0">
    <div class="bg-footer">
        <img src="img/1-main/main-bg-1.png">
    </div>
    <div class="copyright bg-main">
        <div class="container text-center py-2">
            <p class="mb-0 text-2 text-light">บริษัท เทเวศประกันภัย จำกัด (มหาชน) The Deves Insurance Public Company Limited สงวนลิขสิทธิ์ตาม พรบ. ลิขสิทธิ์ พศ.2564 Copyright 2021</p>
        </div>
    </div>
</footer>