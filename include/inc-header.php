<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyChangeLogo': true, 'stickyStartAt': 60, 'stickyHeaderContainerHeight': 60}">
    <div class="header-container container-fluid" style="height:80px">
        <div class="header-row">
            <div class="header-column">
                <div class="header-row">
                    <div class="header-logo mt-0 mb-0">
                        <a href="index.php">
                            <img  height="140" data-sticky-height="100" src="img/1-main/logo.png">
                        </a>
                    </div>
                    <div class="header-nav header-nav-stripe">
                        <div class="menu-main header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
                            <nav class="collapse">
                                <ul class="nav nav-pills" id="mainNav">
                                    <li class="">
                                        <a class="" href="index.php">
                                            <img src="img/1-main/home.svg" class="icon-m" alt="">
                                            <img src="img/1-main/home-white.svg" class="icon-w" alt="">
                                            &nbsp;หน้าแรก
                                        </a>
                                    </li>
                                    <li class="">
                                        <a class="" href="#">
                                            <img src="img/1-main/course.svg" class="icon-m" alt="">
                                            <img src="img/1-main/course-white.svg" class="icon-w" alt="">
                                            &nbsp;หลักสูตร
                                        </a>
                                    </li>
                                    <li class="">
                                        <a class="" href="#">
                                            <img src="img/1-main/calendar.svg" class="icon-m" alt="">
                                            <img src="img/1-main/calendar-white.svg" class="icon-w" alt="">
                                            &nbsp; กิจกรรมข่าวสาร
                                        </a>
                                    </li>
                                    <li class="">
                                        <a class="" href="#">
                                            <img src="img/1-main/faq.svg" class="icon-m" alt="">
                                            <img src="img/1-main/faq-white.svg" class="icon-w" alt="">
                                            &nbsp;คำถามที่พบบ่อย
                                        </a>
                                    </li>
                                    <li class="">
                                        <a class="" href="#">
                                            <img src="img/1-main/usability.svg" class="icon-m" alt="">
                                            <img src="img/1-main/usability-white.svg" class="icon-w" alt="">
                                            &nbsp;วิธีการใช้งาน
                                        </a>
                                    </li>
                                    <li class="">
                                        <a class="" href="#">
                                            <img src="img/1-main/contact.svg" class="icon-m" alt="">
                                            <img src="img/1-main/contact-white.svg" class="icon-w" alt="">
                                            &nbsp;ติดต่อเรา
                                        </a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                        <button class="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>
                    <div class="header-nav-features">
                        <div class="header-nav-features-search-reveal-container">
                            <div class="search-btn header-nav-feature header-nav-features-search header-nav-features-search-reveal d-inline-flex">
                                <a href="#" class="header-nav-features-search-show-icon d-inline-flex text-decoration-none"><i class="fas fa-search header-nav-top-icon"></i></a>
                            </div>
                            <li class="language-change nav-item dropdown nav-item-left-border d-none d-sm-block nav-item-left-border-remove nav-item-left-border-md-show">
                                <a class="nav-link main-flag" href="#" role="button" id="dropdownLanguage" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="img/1-main/th-flag.png" />
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                                    <a class="dropdown-item" href="#"><img src="img/1-main/en-flag.png" alt="EN" /> EN</a>
                                    <a class="dropdown-item" href="#"><img src="img/1-main/th-flag.png" alt="TH" /> TH</a>
                                </div>
                            </li>
                            <a href="login.php" target="_blank" class="btn-login line-height-2 ms-2 d-sm-inline-block">
                                <img src="img/1-main/login-icon.png" alt=""> เข้าสู่ระบบ
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-row">
        <div class="header-nav-features header-nav-features-no-border p-static z-index-2">
            <div class="header-nav-feature header-nav-features-search header-nav-features-search-reveal header-nav-features-search-reveal-big-search px-3">
                <form role="search" class="d-flex w-100 h-100" action="page-search-results.html" method="get">
                    <div class="big-search-header input-group">
                        <input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="พิมพ์คำค้นหา">
                        <a href="#" class="header-nav-features-search-hide-icon"><i class="fas fa-times header-nav-top-icon"></i></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</header>