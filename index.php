<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="banner-index">
                <div class="owl-carousel owl-theme nav-inside nav-style-1 nav-light" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block">
                            <lottie-player src="img/1-main/animation/banner-animation.json" loop autoplay background="transparent" speed="1" style="width: 100%; height: auto;"></lottie-player>
                        </div>
                    </div>

                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block">
                            <img class="img-fluid border-radius-0" src="img/1-main/banner-img.jpg" alt="">
                        </div>
                    </div>
                </div>
            </section>

            <section class="menu-index">
                <div class="featured-boxes featured-boxes-style-2 menu-elearning">
                    <div class="container">
                        <div class="owl-carousel owl-theme nav-inside nav-style-1 nav-light" data-plugin-options="{'items': 5, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
                            <di>
                                <div class="featured-box featured-box-one  featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured"><img src="img\1-main\main-icon-1.png"></div>
                                        </a>
                                        <h4 class="mb-0">ห้องเรียนออนไลน์</h4>
                                    </div>
                                </div>
                            </di>

                            <di>
                                <div class="featured-box featured-box-two featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured"><img src="img\1-main\main-icon-2.png"></div>
                                        </a>
                                        <h4 class="mb-0">e-Library</h4>
                                    </div>
                                </div>
                            </di>

                            <div>
                                <div class="featured-box featured-box-three featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured"><img src="img\1-main\main-icon-3.png"></div>
                                        </a>
                                        <h4 class="mb-0">เอกสารดาวน์โหลด</h4>
                                    </div>
                                </div>
                            </div>

                            <di>
                                <div class="featured-box featured-box-four featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured"><img src="img\1-main\main-icon-4.png"></div>
                                        </a>
                                        <h4 class="mb-0">แผนการเรียน</h4>
                                    </div>
                                </div>
                            </di>

                            <div>
                                <div class="featured-box featured-box-five featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured "><img src="img\1-main\main-icon-5.png"></div>
                                        </a>
                                        <h4 class="mb-0">สถานะการเรียน</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="course-index">
                <div class="bg-course">
                    <img src="img/1-main/main-bg-1.png">
                </div>
                <div class="container pb-5">
                    <div class="row mt-5">
                        <div class="col">
                            <h3 class="title-panel"><span>หลักสูตรของเรา</span></h3>
                            <div class="owl-carousel owl-theme show-nav-title" data-plugin-options="{'items': 3, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
                                <?php for ($i = 0; $i < 8; $i++) { ?>
                                    <div class="py-4 px-1">
                                        <div class="card card-course">
                                            <a href="#">
                                                <img class="card-img-top" src="img/1-main/thumbnail.png">
                                            </a>
                                            <div class="card-body">
                                                <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> หมวดหมู่</p>
                                                <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                                <hr class="mb-2">
                                                <span class="card-text "><i class="icon-clock"></i> 1 ชั่วโมง 30 นาที</span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="text-end">
                        <a href="#" class="btn-viewall">
                            <span class="h5">ดูหลักสูตรทั้งหมด <img src="img/1-main/viewall.svg"></span>
                        </a>
                    </div>
                </div>
            </section>

            <section class="activity-index">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-8 mt-2 ">
                            <h3 class="title-panel-white">กิจกรรมข่าวสาร</h3>
                            <div class="card">
                                <div class="row p-3">
                                    <?php for ($i = 0; $i < 3; $i++) { ?>
                                        <div class="col-lg-4 mb-4">
                                            <article class="post">
                                                <div class="post-image">
                                                    <a href="#">
                                                        <img src="img/1-main/thumbnail.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-1 mb-2" alt="" />
                                                    </a>
                                                </div>
                                                <div>
                                                    <div class="blog-detail">
                                                        <p class="mb-1 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam nisi ultricies nisi luctus, sed fermentum. </p>
                                                        <p class="text-color-muted text-2 mb-1"><i class="icon-calendar"></i> 10 เมษายน 2564</p>
                                                        <a href="#" class="read-more  text-2" style="float: right;">อ่านเพิ่มเติม<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="text-end mt-3">
                                <a href="#" class="btn-viewall">
                                    <span class="h5">ดูกิจกรรมทั้งหมด <img src="img/1-main/viewall.svg"></span>
                                </a>
                            </div>
                        </div><!-- style="background-image: url('img/1-main/main-bg-2.png');" -->
                        <div class="col col-lg-4 mt-2" >
                            <h3 class="title-panel-white">วีดีโอแนะนำ</h3>
                            <div class="p-3 card">
                                <article class="post">
                                    <div class="post-image">
                                        <a href="#">
                                            <video class="w-100" controls>
                                                <source src="video/deves-video.mp4"  type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>
                                            <!-- <img src="img/1-main/thumbnail.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0 mb-2" alt="" /> -->
                                        </a>
                                    </div>
                                    <div class="text-center">
                                        <h4><a href="#" class="text-decoration-none">หลักสูตรทดลองเรียน</a></h4>
                                    </div>
                                </article>
                            </div>
                            <div class="text-end mt-3">
                                <a href="#" class="btn-viewall">
                                    <span class="h5">ดูวีดีโอทั้งหมด <img src="img/1-main/viewall.svg"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>