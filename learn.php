<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .learn-navbar {
            border-bottom: 1px solid #DADADA;
            background-color: #ffffff;
            z-index: 10;
            position: sticky;
            top: 0;
        }

        #note {
            border-left: 1px solid #DADADA;
            height: 100vh;
            background-color: #ffffff;
            z-index: 10;
            position: fixed;
            top: 57px;
            right: 0;
        }

        .note-sm {
            width: 70vw;
            left: 100vw;
        }

        .transition {
            transition: 0.5s;
        }

        .open-note-sm {
            transform: translatex(-70vw);
        }

        .close-section {
            transform: translatex(100vw);
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">

        <div role="main" class="main">

            <div class="learn-navbar">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-between py-2 ">
                        <div class="col-auto">
                            <a href="./coursedetail.php"><button>กลับไปหน้าหลักสูตร</button></a>
                        </div>
                        <div class="col-auto">
                            <div class="btn-group" role="group" aria-label="Basic checkbox toggle button group">
                                <input type="checkbox" class="btn-check" id="btncheck1" autocomplete="off" onclick="slideAction();">
                                <label class="btn btn-primary" for="btncheck1"><img src=".\img\1-main\slide-icon.png"> <span class="d-none d-md-inline">สไลด์เนื้อหาวิดีโอ</span></label>
                                <input type="checkbox" class="btn-check" id="btncheck2" autocomplete="off" onclick="noteAction();">
                                <label class="btn btn-primary" for="btncheck2"><img src=".\img\1-main\note-icon.png"><span class="d-none d-md-inline">จดบันทึก</span></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row p-0 g-0 flex-nowrap">

                <div id="video-main" class="col-12 transition p-5 mb-4">
                    <div class="row flex-lg-nowrap">

                        <div id="video-container" class="col-12 transition mb-4">
                            <div class="ratio ratio-4x3">
                                <video id="video" width="100%" height="100%" muted loop preload="metadata" poster="./video/presentation.jpg">
                                    <source src="./video/movie.ogv" type="video/ogv">
                                    <source src="./video/movie.mp4" type="video/mp4">
                                </video>
                                <a href="#" class="position-absolute top-50pct left-50pct transform3dxy-n50 bg-light rounded-circle d-flex align-items-center justify-content-center text-decoration-none bg-color-hover-primary text-color-hover-light play-button-lg pulseAnim pulseAnimAnimated" data-trigger-play-video="#video" data-trigger-play-video-remove="yes">
                                    <i class="fas fa-play text-5"></i>
                                </a>
                            </div>
                        </div>

                        <div id="slide" class="transition close-section d-none d-lg-inline">
                            <figure>
                                <img alt="" class="img-fluid rounded" src="https://via.placeholder.com/700x520">
                            </figure>
                        </div>

                    </div>
                    <div class="owl-carousel owl-theme stage-margin my-3">
                        <div>
                            <img alt="" class=" img-fluid rounded" src="img/projects/project-5.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-6.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-7.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-1.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-2.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-3.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-1.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-2.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-3.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-fluid rounded" src="img/projects/project-4.jpg">
                        </div>
                    </div>
                </div>

                <div id="note" class="transition close-section">
                    <div class="row m-0 border-bottom p-3">
                        <form action="#">
                            <h6>จดบันทึก</h6>
                            <textarea id="message" placeholder="พิมพ์ข้อความ" rows="4" class="w-100 p-2"></textarea>
                            <input type="submit" value="บันทึก" class="float-end my-3 px-3">
                        </form>
                    </div>
                    <div class="row m-0 p-3">
                        <div class="col-12">
                            <h6>รายการบันทึกของคุณ</h6>
                            <p><span> 01:20 </span> &nbsp; ข้อความ1</p>
                            <hr>
                            <p><span> 01:20 </span> &nbsp; ข้อความ1</p>
                            <hr>
                            <p><span> 01:20 </span> &nbsp; ข้อความ1</p>
                            <hr>
                        </div>
                    </div>
                </div>
                
            </div>


        </div>
    </div>
    <?php include 'include/inc-script.php'; ?>

    <script>
        var chkMedia = window.matchMedia("(max-width: 767px)");
        noteResponsive(chkMedia); // Call listener function at run time
        chkMedia.addListener(noteResponsive); // Attach listener function on state changes

        function noteResponsive(chkMedia) {
            var note = document.getElementById("note"),
                main = document.getElementById("video-main");
            if (chkMedia.matches) { note.className = "transition note-sm";}
            else { 
                note.className = "transition close-section";
                main.className = "col-12 transition p-5 mb-4";
            }
        }

        function noteAction() {
            var noteButton, main, note, x;
            noteButton = document.getElementById("btncheck2");
            main = document.getElementById("video-main");
            note = document.getElementById("note");
            x = window.matchMedia("(max-width: 767px)");

            if (x.matches) {
                noteButton.checked ? note.className += " open-note-sm" : note.className = note.className.replace(" open-note-sm", "");
            } else {
                if (noteButton.checked) {
                    main.className = main.className.replace("col-12", "col-md-8 col-lg-9");
                    note.className = note.className.replace("close-section", "col-10 col-md-4 col-lg-3");
                } else {
                    main.className = main.className.replace("col-md-8 col-lg-9", "col-12");
                    note.className = note.className.replace("col-10 col-md-4 col-lg-3", "close-section");
                }
            }
        }

        function slideAction() {
            var slideButton, main, note;
            slideButton = document.getElementById("btncheck1");
            video = document.getElementById("video-container");
            slide = document.getElementById("slide");
            if (slideButton.checked) {
                video.className = video.className.replace("col-12", "col-lg-6");
                slide.className = slide.className.replace("close-section d-none d-lg-inline", "col-lg-6");
            } else {
                video.className = video.className.replace("col-lg-6", "col-12");
                slide.className = slide.className.replace("col-lg-6", "close-section d-none d-lg-inline");
            }
        }

        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 10,
            loop: false,
            nav: true,
            dots: false,
            // autoWidth: true,
            // autoHeight: true,
            stagePadding: 40,
            responsive: {
                0: {
                    items: 3,
                },
                500: {
                    items: 5,
                },
                768: {
                    items: 7,
                }
            }
        });
    </script>

</body>

</html>