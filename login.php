<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>

    <style>
        #branding {
            background-image: url(./img/1-main/login-bg.png);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: left bottom;
        }
    </style>

</head>

<body data-plugin-page-transition>

    <div class="body">
        <div role="main" class="main">

            <div class="header-logo mt-0 mb-0">
                <a href="index.php">
                    <img alt="Porto" class="position-fixed d-none d-md-block" style="left:70px;" height="150" src="./img/1-main/logo.png">
                </a>
            </div>

            <div class="row vh-100">
                <div id="branding" class="col-md-7 d-none d-md-block"></div>
                <div class="col col-md-5  float-end">
                    <div class="login-section">
                        <a class="text-decoration-none" href="index.php"><i class="fas fa-chevron-left text-1 me-1"></i>กลับหน้าแรก</a>
                        <h3 class="mt-5 pt-5">เข้าสู่ระบบ</h3>
                        <form action="/" id="frmSignIn" method="post" class="needs-validation">
                            <div class="row align-items-center g-3">
                                <div class="form-group col-auto">
                                    <img src=".\img\1-main\username-icon.png">
                                </div>
                                <div class="form-group col">
                                    <input type="text" value="" class="form-control form-control-lg" placeholder="ชื่อผู้ใช้งาน" required>
                                </div>
                            </div>
                            <div class="row align-items-center g-3">
                                <div class="form-group col-auto">
                                    <img src=".\img\1-main\password-icon.png">
                                </div>
                                <div class="form-group col">
                                    <input type="password" value="" class="form-control form-control-lg" placeholder="รหัสผ่าน" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col ">
                                    <a class="text-decoration-none text-color-dark text-color-hover-primary text-3 float-end" href="forgotpassword.php">ลืมรหัสผ่าน?</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <button type="submit" class="btn btn-main w-100 text-4 py-2 my-4" data-loading-text="Loading...">เข้าสู่ระบบ</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    </div>
    </div>

    <?php include 'include/inc-script.php'; ?>
</body>

</html>