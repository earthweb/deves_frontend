<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">ชื่อกิจกรรมข่าวสาร</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li class="active">ชื่อกิจกรรมข่าวสาร</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="position-relative">

                <div class="bg-page">
                    <img src="img/1-main/main-bg-1.png">
                </div>

                <div class="container content news">
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <img src="img/projects/project-portfolio-4-1.jpg">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-8 my-5">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi accusantium corporis in dolorem! Dolorum nihil molestias in quod iure molestiae officiis ipsam dicta. Veniam consequatur at, cum est sit qui!</p>
                            <div class="d-flex justify-content-center dots">
                                <span></span><span></span><span></span>
                            </div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi accusantium corporis in dolorem! Dolorum nihil molestias in quod iure molestiae officiis ipsam dicta. Veniam consequatur at, cum est sit qui!</p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <img src="img/projects/project-2.jpg">
                            <p class="img-ref">"อ้างอิงรูปภาพ"</p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-8 my-5">
                            <blockquote>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus quia, illum, delectus ratione impedit commodi, accusamus dolores beatae tenetur perferendis nobis possimus. Laboriosam cupiditate mollitia esse dolorem officiis necessitatibus unde?</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <span class="tag">#TAG</span>
                            <hr class="mt-5 mb-4">
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-6 order-md-2 mb-5">
                            <div class="row justify-content-center g-4">
                                <div class="col-auto">
                                    <a href="#" title="Facebook">
                                        <div class="feature-box feature-box-style-6">
                                            <div class="feature-box-icon">
                                                <img src=".\img\1-main\facebook-icon.png">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-auto">
                                    <a href="#" title="Twitter">
                                        <div class="feature-box feature-box-style-6">
                                            <div class="feature-box-icon">
                                                <img src=".\img\1-main\twitter-icon.png">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-auto">
                                    <a href="#" title="Line">
                                        <div class="feature-box feature-box-style-6">
                                            <div class="feature-box-icon">
                                                <img src=".\img\1-main\line-icon.png">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 order-md-1">
                            <a href="./allnews.php" class="btn btn-outline btn-rounded btn-light text-2 px-4">
                                <i class="fas fa-chevron-left"></i> ย้อนกลับ
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>