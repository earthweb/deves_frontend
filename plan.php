<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .table-plan-container {
            overflow-x: auto;
        }

        .table-plan {
            border-collapse: collapse;
            display: grid;
            width: 100%;
            grid-template-columns: minmax(400px, 3fr) repeat(12, minmax(70px, 1fr));
            grid-template-rows: 60px;
        }

        .cell {
            border: 1px solid #dddddd;
            padding: 15px 5px;
            text-align: center;
        }

        .cell:nth-of-type(13n + 1) {
            grid-column: 1/1;
            text-align: start;
            padding: 15px 20px;
        }

        .cell:nth-of-type(13n + 2) {
            grid-column: 2/2;
        }

        .cell:nth-of-type(13n + 3) {
            grid-column: 3/3;
        }

        .cell:nth-of-type(13n + 4) {
            grid-column: 4/4;
        }

        .cell:nth-of-type(13n + 5) {
            grid-column: 5/5;
        }

        .cell:nth-of-type(13n + 6) {
            grid-column: 6/6;
        }

        .cell:nth-of-type(13n + 7) {
            grid-column: 7/7;
        }

        .cell:nth-of-type(13n + 8) {
            grid-column: 8/8;
        }

        .cell:nth-of-type(13n + 9) {
            grid-column: 9/9;
        }

        .cell:nth-of-type(13n + 10) {
            grid-column: 10/10;
        }

        .cell:nth-of-type(13n + 11) {
            grid-column: 11/11;
        }

        .cell:nth-of-type(13n + 12) {
            grid-column: 12/12;
        }

        .cell:nth-of-type(13n + 13) {
            grid-column: 13/13;
        }

        .th {
            font-weight: bold;
            border-top: 2px solid #dddddd;
            border-bottom: 2px solid #dddddd;
            background-color: #FBFBFB;
        }

        .event {
            padding: 0 10px;
            position: relative;
            align-self: center;
            color: #ffffff;

        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">แผนการเรียน</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li class="active">แผนการเรียน</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="py-5">
                <h4 class="mx-5">>> ปี 2564<span> : ระยะเวลาที่สามารถเรียนได้แต่ละหลักสูตร</span></h4>
                <div class="mx-5 my-4">
                    <div class="table-plan-container">
                        <div id="table-plan" class="table-plan">
                            <div class='cell th'>ชื่อหลักสูตร</div>
                            <div class='cell th'>ม.ค.</div>
                            <div class='cell th'>ก.พ.</div>
                            <div class='cell th'>มี.ค.</div>
                            <div class='cell th'>เม.ย.</div>
                            <div class='cell th'>พ.ค.</div>
                            <div class='cell th'>มิ.ย.</div>
                            <div class='cell th'>ก.ค.</div>
                            <div class='cell th'>ส.ค.</div>
                            <div class='cell th'>ก.ย.</div>
                            <div class='cell th'>ต.ค.</div>
                            <div class='cell th'>พ.ย.</div>
                            <div class='cell th'>ธ.ค.</div>
                            <div class='cell' style='grid-row:2;'>หลักสูตร 1</div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:2;'></div>
                            <div class='cell' style='grid-row:3;'>หลักสูตร 2</div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <div class='cell' style='grid-row:3;'></div>
                            <section class='event' style='grid-row:2; grid-column: 4 / span 5; background-color:#FFA74A;'> 1 มี.ค. - 31 ก.ค. </section>
                            <section class='event' style='grid-row:3; grid-column: 5 / 13; background-color:#4BBC99;'> 1 เม.ย. - 30 พ.ย. </section>
                        </div>
                    </div>
                </div>
            </div>

            <?php include 'include/inc-footer.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>
    </div>
</body>

</html>