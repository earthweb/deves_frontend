<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">สอบหลังเรียน</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li><a href="#">หลักสูตรทั้งหมด</a></li>
                                <li class="active">หลักสูตร 1</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section-main">
                <div class="container py-5">
                    <div class="row justify-content-center">
                        <div class="col col-md-7 col-lg-5">
                            <h4>สอบหลังเรียน บทเรียนที่ 1</h4>
                            <div class="card mb-4">
                                <div class="card-body row p-4">
                                    <div class="col">
                                        <p class="mb-1 text-3">ระยะเวลาทำแบบทดสอบ</p>
                                        <h6 class="text-3"><img src=".\img\1-main\clock-icon-sm.png"> 30 นาที</h6>
                                    </div>
                                    <hr class="mt-4">
                                    <div class="col">
                                        <p class="mb-1 text-3">จำนวนข้อสอบ</p>
                                        <h6 class="text-3">15 ข้อ</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="./pretest-2.php" class="btn btn-main text-decoration-none">
                                    ทำข้อสอบหลังเรียน
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php include 'include/inc-footermain.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>


</body>

</html>