<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
   
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">สอบหลังเรียน</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li><a href="#">หลักสูตรทั้งหมด</a></li>
                                <li class="active">หลักสูตร 1</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container py-5">

                <div class="row">
                    <div class="timer">เวลาทำข้อสอบ: &nbsp<span> 00:30</span></div>
                </div>

                <div class="row justify-content-center my-5">
                    <div class="col-lg-9">
                        <div class="owl-carousel owl-theme stage-margin nav-style-1 mb-0">
                            <?php for ($nav = 1; $nav <= 20; $nav++) { ?>
                                <div class='exam-nav'>
                                    <div id="nav-<?= $nav ?>" class="nav-item" onclick="selectExam(event,<?= $nav ?>);"> <?= $nav ?> </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <hr>

                <div>
                    <form action="#">
                        <?php for ($exam = 1; $exam <= 20; $exam++) { ?>
                            <div id='exam-<?= $exam ?>' class='row justify-content-center my-5 exam'>
                                <div class='col-12'>
                                    <h4>ข้อ <?= $exam ?></h4>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vel iusto deleniti, doloremque recusandae dolore sit exercitationem voluptate et beatae sunt, ex qui earum aliquam at repudiandae modi voluptatibus officiis porro.</p>

                                    <!-- choice -->
                                    <div id='choiceGroup-<?= $exam ?>' class='px-2'>
                                        <?php for ($choice = 1; $choice <= 4; $choice++) { ?>
                                            <input type='radio' name='exam-<?= $exam ?>' id='ans-<?= $choice ?>' autocomplete='off' class='btn-check'>
                                            <label for='ans-<?= $choice ?>' class='row align-items-center choice-item p-3 my-3' onclick='selectChoice(event,<?= $exam ?>)'>
                                                <div class='col-auto'>
                                                    <div class='choice-num text-center pt-1'> <?= $choice ?> </div>
                                                </div>
                                                <div class='col'>Lorem ipsum dolor sit amet consectetur adipisicing elit.</div>
                                            </label>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                        <div class="row justify-content-between">
                            <div class="col-auto">
                                <button class="btn btn-outline btn-rounded btn-dark" id="prev-exam" onclick="changeExam('prev'); return false;">
                                <i class="fas fa-chevron-left"></i> ข้อก่อนหน้า </button>
                            </div>
                            <div class="col-auto"><button class="btn btn-outline btn-rounded btn-dark " id="next-exam" onclick="changeExam('next'); return false;"> ข้อต่อไป <i class="fas fa-chevron-right"></i> </button></div>
                        </div>
                    </form>
                </div>

            </div>

            <?php include 'include/inc-footermain.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>
    </div>

    <script>
        document.getElementById("exam-1").style.display = "block";
        document.getElementById("nav-1").className += " nav-active";
        document.getElementById("prev-exam").style.display = "none";
        var currentExam = 1;

        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 0,
            loop: false,
            nav: true,
            dots: false,
            autoHeight: true,
            stagePadding: 40,
            responsive: {
                0: {
                    items: 3,
                    slideBy: 3,
                },
                500: {
                    items: 5,
                    slideBy: 5,
                },
                768: {
                    items: 10,
                    slideBy: 10,
                }
            }
        });

        function selectChoice(evt, parentIdNum) {
            var i, choice, parentId, navId, nav;
            parentId = "choiceGroup-" + parentIdNum.toString();
            choice = document.getElementById(parentId).children;
            navId = "nav-" + parentIdNum.toString();
            nav = document.getElementById(navId)

            for (i = 0; i < choice.length; i++) {
                choice[i].className = choice[i].className.replace(" choice-check", "");
            }
            evt.currentTarget.className += " choice-check";

            nav.className.replace(" nav-check", "");
            nav.className += " nav-check";
        }

        function selectExam(evt, examIdNum) {
            var i, exam, examId, nav;
            exam = document.getElementsByClassName("exam");
            examId = "exam-" + examIdNum.toString();
            nav = document.getElementsByClassName("nav-item");
            currentExam = examIdNum;

            for (i = 0; i < exam.length; i++) {
                exam[i].style.display = "none";
            }
            document.getElementById(examId).style.display = "block";

            for (i = 0; i < nav.length; i++) {
                nav[i].className = nav[i].className.replace(" nav-active", "");
            }
            evt.currentTarget.className += " nav-active";

            checkPrevNext();
        }

        function changeExam(action) {
            var i, exam, nav, navCurrent;
            action == "prev" ? --currentExam : ++currentExam;
            exam = document.getElementsByClassName("exam");
            examId = "exam-" + currentExam.toString();
            nav = document.getElementsByClassName("nav-item");
            navId = "nav-" + currentExam.toString();
            navCurrent = document.getElementById(navId)

            for (i = 0; i < exam.length; i++) {
                exam[i].style.display = "none";
            }
            document.getElementById(examId).style.display = "block";

            for (i = 0; i < nav.length; i++) {
                nav[i].className = nav[i].className.replace(" nav-active", "");
            }
            navCurrent.className += " nav-active";

            checkPrevNext();
        }

        function checkPrevNext() {
            if (currentExam == 1) {
                document.getElementById("prev-exam").style.display = "none";
            } else if (currentExam == 20) {
                document.getElementById("next-exam").style.display = "none";
            } else {
                document.getElementById("prev-exam").style.display = "inline";
                document.getElementById("next-exam").style.display = "inline";
            }
        }
    </script>

</body>

</html>