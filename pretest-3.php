<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        hr.style1 {
            border-top: 1px dashed rgba(8, 8, 8, 0.1);
            margin: 10px 0 22px;
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">สอบหลังเรียน</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li><a href="#">หลักสูตรทั้งหมด</a></li>
                                <li class="active">หลักสูตร 1</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col col-lg-7">
                        <div class="card mb-4">
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <img src=".\img\1-main\check_circle_24px.png">
                                    <h6 class="text-3 mt-2">สอบผ่าน</h6>
                                    <h4>สรุปผลสอบ บทเรียนที่ 1</h4>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col">
                                        <h6>จำนวนข้อสอบทั้งหมด</h6>
                                    </div>
                                    <div class="col text-end">
                                        <h6>15 ข้อ</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>เวลาในการทำข้อสอบ</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6>30 นาที</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>ใช้เวลาในการสอบ</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6>15 นาที</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>คะแนนเต็ม</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6>15 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>คะแนนที่ได้</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6>13 คะแนน</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>คิดเป็นร้อยละ</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6>90 %</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="./coursedetail.php" class="btn btn-main text-decoration-none mx-auto d-block">
                           กลับหน้าหลักสูตร
                        </a>
                    </div>
                </div>

            </div>

            <?php include 'include/inc-footermain.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>


</body>

</html>