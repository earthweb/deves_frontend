<!DOCTYPE html>
<html>

<head>
    <title>ข้อมูลส่วนตัว</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">ข้อมูลส่วนตัว</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="./index.php">หน้าแรก</a></li>
                                <li class="active">ข้อมูลส่วนตัว</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="row g-0 position-relative">

                <div class="bg-page">
                    <img src="img/1-main/main-bg-1.png">
                </div>

                <div class="sidebar col-md-3 col-lg-2">
                    <div class="container-fluid">
                        <p class="my-0">ข้อมูลส่วนตัว</p>
                        <hr class="my-2">
                        <p class="my-0"><a class="text-decoration-none" href="./status-user.php">สถานะการเรียน</a></p>
                        <hr class="my-2">
                    </div>
                </div>

                <div class="content col col-md-9 col-lg-10">
                    <div class="container pb-5">
                        <div class="row justify-content-center">
                            <div class="col col-md-10 col-lg-8">
                                <div class="card card-profile border">
                                    <!-- <div class="card-body"> -->
                                    <p>ชื่อ-นามสกุล : &emsp;&emsp;&emsp;<span>อัศวรรณ์ จำเริญสม</span></p>
                                    <p>เลขที่บัตรประชาชน : &emsp;<span> 7489287894756</span></p>
                                    <p>เลขที่ใบอนุญาติ : &emsp;&emsp;<span> XXXXXXXXX</span></p>
                                    <p>เลขที่พนักงาน : &emsp;&emsp;&emsp;<span>XXXXXXXXX</span></p>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>