<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .table-questionnaire {
            min-width: 700px;
            text-align: center;
        }

        .table-questionnaire thead tr th {
            vertical-align: middle;
        }

        .table-questionnaire tbody tr td:nth-child(2) {
            text-align: left;
            width: 300px;
        }

        .table-questionnaire td,
        .table-questionnaire th {
            width: 75px;
        }

        #suggestion {
            width: 100%;
            height: 100px;
            padding: 10px;
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">แบบประเมินหลักสูตร</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li class="active">แบบประเมินหลักสูตร</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="row">
                    <h4 class="mb-0">>>แบบประเมินความพึงพอใจหลักสูตร</h4>
                    <form>
                        <div class="col table-responsive my-4">
                            <table class="table table-bordered table-questionnaire">
                                <thead>
                                    <tr>
                                        <th rowspan="2">ลำดับ</th>
                                        <th rowspan="2" class="detail">หัวข้อประเมิน</th>
                                        <th colspan="5">ระดับคะแนน</th>
                                    </tr>
                                    <tr>
                                        <th>5<br>ดีมาก</th>
                                        <th>4<br>ดี</th>
                                        <th>3<br>ปานกลาง</th>
                                        <th>2<br>พอใช้</th>
                                        <th>1<br>ปรับปรุง</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="1" value="5"></td>
                                        <td><input type="radio" name="1" value="4"></td>
                                        <td><input type="radio" name="1" value="3"></td>
                                        <td><input type="radio" name="1" value="2"></td>
                                        <td><input type="radio" name="1" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="2" value="5"></td>
                                        <td><input type="radio" name="2" value="4"></td>
                                        <td><input type="radio" name="2" value="3"></td>
                                        <td><input type="radio" name="2" value="2"></td>
                                        <td><input type="radio" name="2" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="3" value="5"></td>
                                        <td><input type="radio" name="3" value="4"></td>
                                        <td><input type="radio" name="3" value="3"></td>
                                        <td><input type="radio" name="3" value="2"></td>
                                        <td><input type="radio" name="3" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col card mb-5">
                            <div class="card-body">
                                <h6>ความคิดเห็นเพิ่มเติม หรือข้อแนะนำ</h6>
                                <textarea id="suggestion" placeholder="คำตอบของคุณ"></textarea>
                            </div>
                        </div>
                        <div class="col text-center mb-5">
                            <a href="#"><button>ส่งแบบประเมิน</button></a>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>

</body>

</html>