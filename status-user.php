<!DOCTYPE html>
<html>

<head>
    <title>สถานะการเรียน</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">สถานะการเรียน</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li class="active">สถานะการเรียน</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="row g-0 position-relative">

                <div class="bg-page">
                    <img src="img/1-main/main-bg-1.png">
                </div>

                <div class="sidebar col-md-3 col-lg-2">
                    <div class="container-fluid">
                        <p class="my-0"><a href="./profile.php">ข้อมูลส่วนตัว</a></p>
                        <hr class="my-2">
                        <p class="my-0">สถานะการเรียน</p>
                        <hr class="my-2">
                    </div>
                </div>

                <div class="content col col-md-9 col-lg-10">
                    <div class="container pb-5">
                        <div class="row g-5">

                            <div class="col-sm-6 col-lg-4">
                                <div class="card card-course">
                                    <a href="#"><img class="card-img-top" src="img/1-main/thumbnail.png"></a>
                                    <div class="card-body" style="padding:10px;">
                                        <h6 class="card-title mb-2 text-4 text-main "><a href="#">ชื่อหลักสูตร</a></h6>
                                        <div class="progress progress-sm progress-border-radius mt-4 mb-2">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                            </div>
                                        </div>
                                        <p class="text-dark mb-2">60 % เสร็จสมบูรณ์ </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>