<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern pb-3 mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static text-center mb-4">
                            <h1 class="text-light">วิดีโอแนะนำ</h1>
                        </div>
                        <div class="col-md-12 align-self-center ">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">หน้าแรก</a></li>
                                <li class="active">วิดีโอแนะนำ</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="row g-5">

                    <div class="col-lg-4 mb-4">
                        <article class="card border-2 p-3 p-md-4">
                            <div class="ratio ratio-4x3">
                                <video id="video1" width="100%" height="100%" muted loop preload="metadata" poster="./video/presentation.jpg">
                                    <source src="./video/movie.ogv" type="video/ogv">
                                    <source src="./video/movie.mp4" type="video/mp4">
                                </video>
                                <a href="#" class="position-absolute top-50pct left-50pct transform3dxy-n50 bg-light rounded-circle d-flex align-items-center justify-content-center text-decoration-none bg-color-hover-primary text-color-hover-light play-button-lg pulseAnim pulseAnimAnimated" data-trigger-play-video="#video1" data-trigger-play-video-remove="yes">
                                    <i class="fas fa-play text-5"></i>
                                </a>
                            </div>
                            <div class="text-center mt-3">
                                <p class="text-5"><a href="#" class="text-decoration-none">หลักสูตรทดลองเรียน</a></p>
                            </div>
                        </article>
                    </div>

                </div>
            </div>

        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>